package code;

import java.awt.Color;

public class Board {
 
	public final int ROW = 25;
	public final int COL = 25;
	public Tiles _board[][] = new Tiles[ROW][COL];
	
	
	public Board(){
		
		for(int i = 0; i>ROW; i++){
			for(int j = 0; j>COL; j++){
				_board[i][j] = new Tiles();
			}	
		}
	}
	
	
}
