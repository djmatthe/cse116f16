package code;

import java.util.ArrayList;
import java.util.Random;

public class Clue {
	private ArrayList<Cards> _weapons;
	private ArrayList<Cards> _rooms;
	private ArrayList<Cards> _suspects;
	private ArrayList<Cards> _culprit;
	
	public Clue(){
	}
	
	public void generateCulprit(){
		Random r = new Random();
		_culprit.add(_weapons.get(r.nextInt(6)));  //pulls a random card from weapons arraylist
		_culprit.add(_rooms.get(r.nextInt(9)));  //pulls a random card from rooms arraylist
		_culprit.add(_suspects.get(r.nextInt(6)));  //pulls a random card from suspect arraylist 
	}
	public void addWeapons(){
		_weapons.add(new Cards("Weapon","Wrench"));
		_weapons.add(new Cards("Weapon","Candlestick"));
		_weapons.add(new Cards("Weapon","Lead Pipe"));
		_weapons.add(new Cards("Weapon","Pipe"));
		_weapons.add(new Cards("Weapon","Revolver"));
		_weapons.add(new Cards("Weapon","Knife"));	
	}
	public void addRooms(){
		_rooms.add(new Cards("Room","Study"));
		_rooms.add(new Cards("Room","Kitchen"));
		_rooms.add(new Cards("Room","Hall"));
		_rooms.add(new Cards("Room","Conservatory"));
		_rooms.add(new Cards("Room","Lounge"));
		_rooms.add(new Cards("Room","Ballroom"));
		_rooms.add(new Cards("Room","Dining Room"));
		_rooms.add(new Cards("Room","Library"));
		_rooms.add(new Cards("Room","Billiard Room"));
	}
	public void addSuspects(){
		_suspects.add(new Cards("Suspect","Colonel Mustard"));
		_suspects.add(new Cards("Suspect","Miss Scarlet"));
		_suspects.add(new Cards("Suspect","Professor Plum"));
		_suspects.add(new Cards("Suspect","Mr.Green"));
		_suspects.add(new Cards("Suspect","Mrs.White"));
		_suspects.add(new Cards("Suspect","Mrs.Peacock"));
	}
	public ArrayList<Cards> getWeapons(){
		return _weapons;
	}
	public ArrayList<Cards> getRooms(){
		return _rooms;
	}
	public ArrayList<Cards> getSuspects(){
		return _weapons;
	}
	public ArrayList<Cards> getCulprit(){
		return _culprit;
	}
}
